# Build container images

Helper repo to build container images on Gitlab CI.

## Testing the builds locally

    for I in base esgf_cordex cdo r calculate_index jupyter
    do
      docker build . -f "${I}/Dockerfile"
    done

## License

Copyright (C) 2023 [simevo s.r.l.](https://simevo.com) for [ARPA Lombardia](https://www.arpalombardia.it).

Licensed under the [BSD 3-Clause (`BSD-3-Clause`)](LICENSE) as per [linee guida per l’Acquisizione e il riuso di software per la Pubblica Amministrazione](https://docs.italia.it/italia/developers-italia/lg-acquisizione-e-riuso-software-per-pa-docs/it/stabile/riuso-software/licenze-aperte-e-scelta-di-una-licenza.html#scelta-di-una-licenza).
